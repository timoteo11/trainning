const validate = require('./validate');

let response  = ((status, message) => {
    return {
      statuseCode : status,
      body: JSON.stringify({message})
    }
});

function sumAmount(transactions)
{
  var result = {
    sumDollar: 0,
    sumPesos: 0
  }
  for(var i = 0; i < transactions.length; i++)
  {
    if(transactions[i].status === 'succeeded'){
      switch (transactions[i].currency) {
        case 'USD':
          result.sumDollar+=transactions[i].amount;
          break;
        case 'ARS':
          result.sumPesos+= transactions[i].amount
          break;
      }
    }
  }
  return result;
}

module.exports.main =  (event, context, callback) => {
  
  let transactions = JSON.parse(event.body).transactions;

  if (!validate.totalValues(transactions)){
    callback(null, response(400, 
      {
        message: 'Los parametros ingresados no son correctos'
      }
    ));
  }
  else{
    let sum = sumAmount(transactions);
    callback(null, response(200, {
      balance: {
        amounts: {
          ars: sum.sumPesos,
          usd: sum.sumDollar
        }
      }}
    ));
  }
};